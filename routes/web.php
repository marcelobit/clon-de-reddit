<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
    //return view('welcome',['nombre' =>'Clon de reddit']);

    //return view('welcome')->with('nombre','Clon de reddit');

	//return view('welcome')->withNombre('Clon de reddit');
	//return redirect()->route('posts_path');
//});



/*Route::get('/hola/{nombre}', function($nombre) {
    return "Hola {$nombre}";
});*/



//las rutas que aceptan cualquier path van ultimas {post}


Route::get('/hola/{nombre}','HolaController@hola');


Auth::routes();

Route::group(['middleware' => 'auth'], function(){

	Route::get('/home', 'HomeController@index')->name('home');
	Route::name('create_post_path')->get('/posts/create','PostsController@create');
	Route::name('store_post_path')->post('/posts','PostsController@store');
	Route::name('edit_post_path')->get('/posts/{post}/edit', 'PostsController@edit');
	Route::name('update_post_path')->put('/posts/{post}', 'PostsController@update');	
	Route::name('delete_post_path')->delete('/posts/{post}', 'PostsController@destroy');
});

Route::get('/', 'PostsController@index');

Route::name('posts_path')->get('/posts', 'PostsController@index');

Route::name('post_path')->get('/posts/{post}', 'PostsController@show'); 