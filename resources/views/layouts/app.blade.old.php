<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Reddit Clone</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md12 form-control">
				<h1>Reddit Cloned</h1>
				<h4><a href="{{ route('create_post_path') }}">Create New Post</a></h4>
				<h4><a href="{{ route('posts_path') }}">View All Post</a></h4>
				
			</div>
		</div>
		
		<hr>
		
		@include('layouts._errors')

		@include('layouts._messages')

		@yield('content')

	</div>
</body>
</html>