<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

use \App\Http\Requests\CreatePostRequest;
use \App\Http\Requests\UpdatePostRequest;


use App\Post;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response

     */
    public function index()
    {
        //$posts = Post::all();

        //$posts = Post::orderBy('id','desc')->get(); //muestra el más reciente primero

        $posts = Post::orderBy('id','desc')->paginate(5);


        return view('posts.index')->with(['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post = new Post;

        return view('posts.create')->with(['post' => $post]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        /*$post = new Post;
        $post->title = $request->get('title');
        $post->description = $request->get('description');
        $post->url = $request->get('url');
        $post->save();*/


        //Alternativa a save

        //$post = Post::create($request->only('title','description','url'));

        $post = new Post;

        $post->fill(
            $request->only('title','description','url')
        );

        $post->user_id = auth()->user()->id;    //este es el usuario que inicio session
                                                // $request->user()->id;
                                                // \Aurh::user()->id;
        $post->save();

        session()->flash('message', 'Post Created');

        return redirect()->route('posts_path');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //$post = Post::find($postId);
        return  view('posts.show')->with(['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        if($post->user_id != \Auth::user()->id){

            return redirect()->route('posts_path');
        }
        
        return view('posts.edit')->with(['post'=>$post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Post $post, UpdatePostRequest $request)
    {
        $post->update(
            $request->only('title', 'description', 'url')
        );

        session()->flash('message', 'Post Updated');

        return redirect()->route('post_path',['post' => $post->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        
        // Si el usuario logueado es distinto del que esta intentado hacer el cambio

        if($post->user_id != \Auth::user()->id){

            return redirect()->route('posts_path');
        }

        $post->delete();

        session()->flash('message', 'Post Delete');

        return redirect()->route('posts_path');

    }
}